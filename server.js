const express = require('express');
const cors = require('cors');
const path = require('path');
const fs = require('fs');
const MongoClient = require('mongodb').MongoClient;
const app = express();
const bodyParser = require('body-parser');
app.use(cors());

app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use(bodyParser.json());

// use when starting application locally
const mongoUrlLocal = "mongodb://arlagoz:1234@mongodb:27017";

// use when starting application as docker container
const mongoUrlDocker = "mongodb://arlagoz:1234@mongodb";
//let mongoUrlDocker = "mongodb://username:password@nameofmongocontainer";

// pass these options to mongo client connect request to avoid DeprecationWarning for current Server Discovery and Monitoring engine
const mongoClientOptions = { useNewUrlParser: true, useUnifiedTopology: true };

const databaseName ='user-account'






app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/profile-picture', function (req, res) {
    let img = fs.readFileSync(path.join(__dirname, 'images/profile-1.jpg'));
    res.writeHead(200, { 'Content-Type': 'image/jpg' });
    res.end(img, 'binary');
});

app.get('/get-profile', function (req, res) {
    const response = res;
    MongoClient.connect(mongoUrlDocker, function(err, client){
        if(err) throw err
        const db = client.db(databaseName)
        const query = {userid: 1}
        db.collection('users').findOne(query, function (err, result) {
            if(err) throw err
            client.close()
            response.send(result)
            
        })

    })
});

app.post('/update-profile', function (req, res) {
    let userObj = req.body;
  
    MongoClient.connect(mongoUrlDocker, mongoClientOptions, function (err, client) {
      if (err) throw err;
  
      let db = client.db(databaseName);
      userObj['userid'] = 1;
  
      let myquery = { userid: 1 };
      let newvalues = { $set: userObj };
  
      db.collection("users").updateOne(myquery, newvalues, {upsert: true}, function(err, res) {
        if (err) throw err;
        client.close();
      });
  
    });
    // Send response
    res.send(userObj);
  });
  

app.listen(3000, () => {
    console.log('it starts to listen');
});
