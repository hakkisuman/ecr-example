FROM node:13-alpine

ENV MONGO_DB_USERNAME=KULLANICIADI \
	MONGO_DB_PWD=SIFRE

RUN mkdir -p /home/app

COPY . /home/app

CMD [ "node", "/home/app/server.js"]